<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProofController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application Proof Overview.
     *
     * @return \Illuminate\Http\Response
     */
    public function overview()
    {
        return view('proof.overview');
    }

    /**
     * Show the application Proof Add.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('proof.add');
    }

    /**
     * Show the application Proof Settings.
     *
     * @return \Illuminate\Http\Response
     */
    public function settings()
    {
        return view('proof.settings');
    }
}
