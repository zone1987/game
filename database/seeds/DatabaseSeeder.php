<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $now = new DateTime();

        $users = [
            ['name' => 'Nadine Probst', 'email' => 'n.probst1990@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Simon Probst', 'email' => 's.probst@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Sophia Probst', 'email' => 'omm.probst@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Daniela Odenthal', 'email' => 'd.odenthal@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Tanja Schachtmann', 'email' => 't.schachtmann@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Günther Schachtmann', 'email' => 'g.schachtmann@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Petra Gerhardt', 'email' => 'p.gerhardt@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Ralph Gerhardt', 'email' => 'r.gerhardt@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Sebastian Zimmermann', 'email' => 's.zimmermann@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Sebastian Meine', 'email' => 's.meine@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Daniel Thaller', 'email' => 'd.thaller@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Ronny Stolze', 'email' => 'r.stolze@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Jan Harenbrock', 'email' => 'j.harenbrock@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Jan Kienitz', 'email' => 'j.kienitz@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'Daniela Wotsch', 'email' => 'd.wotsch@gmail.com', 'password' => 'zonen100487'],
            ['name' => 'awesome', 'email' => 'awesome@gmail.com', 'password' => 'awesome']
        ];

        $emailAddresses = $this->findAllRecordsFromRow('users', 'email');

        if (!in_array('a.gerhardt1987@gmail.com', $emailAddresses)) {
            DB::table('users')->insert([
                'name' => 'Andreas Gerhardt',
                'email' => 'a.gerhardt1987@gmail.com',
                'password' => bcrypt('zonen100487'),
                'role' => 2,
                'avatar' => null,
                'blocked' => 0,
                'ghost' => 0,
                'deleted' => 0,
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s')
            ]);
        }
        
        DB::beginTransaction();
        foreach ($users as $user) {
            if (!in_array($user['email'], $emailAddresses)) {
                DB::table('users')->insert([
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'password' => bcrypt($user['password']),
                    'role' => 1,
                    'avatar' => null,
                    'blocked' => 0,
                    'ghost' => 0,
                    'deleted' => 0,
                    'created_at' => $now->format('Y-m-d H:i:s'),
                    'updated_at' => $now->format('Y-m-d H:i:s')
                ]);
            }
        }
        DB::commit();
    }

    private function findAllRecordsFromRow($table, $row) {
        $result = [];
        foreach (DB::table($table)->select(DB::raw($row))->get() as $record) {
            $result[] = collect($record)[$row];
        }
        return $result;
    }
}
