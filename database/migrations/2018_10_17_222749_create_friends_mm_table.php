<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendsMmTable extends Migration
{

    private $name = 'friends_mm';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->name)) {
            Schema::create($this->name, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('friend_left_id')->unsigned();
                $table->integer('friend_right_id')->unsigned();
                $table->foreign('friend_left_id')->references('id')->on('users')->onDelete('cascade');
                $table->foreign('friend_right_id')->references('id')->on('users')->onDelete('cascade');
            });
        }   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->name);
    }
}
