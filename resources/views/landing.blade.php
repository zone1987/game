<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    @auth
        <script>window.location = "/home";</script>
    @else
    <div id="proof">
        <div class="container login">
            <div class="row justify-content-center">
                <div class="col-md-5 login-form">
                    <img src="/images/login.svg" height="100">

                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="E-mail Adresse">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Passwort">
                        
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-5">
                                <div class="form-check">
                                    <input type="checkbox" id="remember" name="set-name" class="switch-input form-check-input" name="remember" data-toggle="toggle" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="remember" class="switch-label">
                                        <span class="toggle--on">Passwort merken</span>
                                        <span class="toggle--off">Passwort nicht merken</span>
                                    </label>
                                </div>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Passwort vergessen?
                                </a>
                            </div>
                        </div>

                        <button type="submit" class="btn">
                            {{ __('Login') }}
                        </button>

                        <a class="btn sign-up" href="{{ route('register') }}" role="button">Registrieren</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @endauth
</body>
</html>
