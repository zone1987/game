@extends('layouts.app')

@section('content')

<div id="proof">
    <div class="container register">
        <div class="row justify-content-center">
            <div class="col-md-5 login-form">
                <img src="/images/register.svg" height="100">

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="E-Mail Adresse">

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Passwort">

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Passwort bestätigen">
                        
                        <button type="submit" class="register btn">
                            Kostenlos Registrieren
                        </button>

                        <a class="btn sign-up" href="{{ URL::previous() }}" role="button">Login</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
