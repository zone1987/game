<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    {!! MaterializeCSS::include_full() !!}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div id="proof" class="dashboard">
            <div id="header">
                <div class="brand">
                    <div class="logo">
        
                    </div>
                    <div class="text">
                        <a href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
                    </div>
                </div>
                <div class="info">
                    
                </div>
                <div class="profil">
                    <div class="name">
                        {{ Auth::user()->name }}
                    </div>
                    <div class="avatar">
        
                    </div>
                    <div class="dropdown">
                        <ul style="color: white;">
                            <li class="waves-effect waves-blue-green">
                                <i class="material-icons">person_outline</i>
                                <span>Profil</span>
                            </li>
                            <li class="waves-effect waves-blue-green">
                                <i class="material-icons">settings</i>
                                <span>Einstellungen</span>
                            </li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <li class="logout waves-effect waves-blue-green">
                                    <i class="material-icons">keyboard_tab</i>
                                    <span>{{ __('Logout') }}</span>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </a>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="main">
                <div id="sidebar">
                    <ul class="top">
                        <li class="waves-effect waves-blue-green" onclick="window.location = '{{ route('home') }}'">
                            <i class="material-icons">home</i>
                            <span>Startseite</span>
                        </li>
                        <li class="subnav">
                            <div class="subnav-link">
                                <i class="material-icons">subject</i>
                                <span>Nachweise</span>
                            </div>
                            <div class="subnav-content">
                                <ul>
                                    <li class="waves-effect waves-blue-green" onclick="window.location = '{{ route('proof.overview') }}'">Übersicht</li>
                                    <li class="waves-effect waves-blue-green" onclick="window.location = '{{ route('proof.add') }}'">Erstellen</li>
                                </ul>
                            </div>
                        </li>
                        <li class="waves-effect waves-blue-green">
                            <i class="material-icons">people</i>
                            <span>Freunde</span>
                        </li>
                    </ul>
                    <ul class="bottom">
                        <li class="waves-effect waves-blue-green" onclick="window.location = '{{ route('proof.settings') }}'">
                            <i class="material-icons">settings</i>
                            <span>Einstellungen</span>
                        </li>
                    </ul>
                </div>
                <div id="content">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            $('#proof #content #add .date input#week')[0].value = getcurrentWeek();
            $('#proof #content #add .date input#year')[0].value = new Date().getFullYear();
            calculateDate();
        });

        const getcurrentWeek = () => {
            let date1 = new Date(new Date().getFullYear(), 0, 1);
            date1.setDate(date1.getDate() - 1);
            let timeDiff = Math.abs(new Date().getTime() - date1.getTime());
            let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
            return Math.ceil(diffDays / 7);
        }

        const calculateDate = () => {
            let year = $('#proof #content #add .date input#year')[0].value;
            let week = $('#proof #content #add .date input#week')[0].value;

            year = year < 2017 ? 2017 : year > 2099 ? 2099 : year; 
            $('#proof #content #add .date input#year')[0].value = year;

            week = week < 1 ? 1 : week > 53 ? 53 : week;
            $('#proof #content #add .date input#week')[0].value = week;

            let from = new Date(year, 0, (1 + (week - 1) * 7));
            console.log(from);
            fromDay = ("00" + from.getDate()).slice(-2);
            fromMonth = ("00" + (from.getMonth() + 1)).slice(-2);
            fromYear = from.getFullYear();
            from = `${fromDay}.${fromMonth}.${fromYear}`;

            let to = new Date(year, 0, (1 + (week - 1) * 7) + 6);
            console.log(to);
            toDay = ("00" + to.getDate()).slice(-2);
            toMonth = ("00" + (to.getMonth() + 1)).slice(-2);
            toYear = to.getFullYear();
            to = `${toDay}.${toMonth}.${toYear}`;
            $('#proof #header .info')[0].innerHTML = 'Woche vom: ' + from + ' bis: ' + to;
        }
    </script>
</body>
</html>
