@extends('layouts.dashboard')

@section('content')
<div class="row">
    @for ($i = 0; $i < 10; $i++)
    <div class="col-lg-2">
        <div class="card blue-grey darken-1">
        <div class="card-content white-text">
            <span class="card-title">Woche {{ $i }}</span>
        </div>
        </div>
    </div>
    @endfor 
</div>
@endsection
