@extends('layouts.dashboard')

@section('content')
    <div id="add">
        <div class="container">
            <form>
                <div class="date row">
                    <div class="input-group date col-md-2">
                        <label for="year">Jahr:</label>
                        <input type="number" id="year" oninput="calculateDate()"  min="2017" max="2099">
                    </div>
                    <div class="input-group date col-md-2">
                        <label for="week">Woche:</label>
                        <input type="number" id="week" oninput="calculateDate()" min="1" max="53">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
